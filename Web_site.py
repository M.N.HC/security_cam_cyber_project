import os
import socket
from OpenSSL import SSL
from flask import Flask, session, render_template, request, redirect, g, url_for, flash, abort


app = Flask(__name__)
app.secret_key = os.urandom(24)
CREATE_NO_WINDOW = 0x08000000
TCP_IP = '192.168.1.28'
TCP_PORT = 12345
BUFFER_SIZE = 1024


context = SSL.Context(SSL.SSLv23_METHOD)
context = ('cert.crt', 'key.key')


@app.route('/', methods=['GET', 'POST'])
def index():
    error = None
    if request.method == 'POST':
        session.pop('user', None)
        if request.form['password'] == 'password' and request.form['username'] == 'May' and \
                (request.form['camera_number'] == '1' or request.form['camera_number'] == '2'
                 or request.form['camera_number'] == '3'):
            session['user'] = request.form['username']
            return redirect(url_for('camera' + request.form['camera_number']))
        else:
            flash('wrong password!')
            error = 'Invalid Credentials. Please try again.'
    return render_template('index.html', error=error)


@app.route('/camera1', methods=['GET', 'POST'])
def camera1():
    error = None
    if g.user:
        if request.method == 'POST':
            if request.form['action'] == 'turn on':
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect(('127.0.0.1', 12345))
                s.send('Turn On')
                s.close()
            elif request.form['action'] == 'turn off':
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect(('127.0.0.1', 12345))
                s.send('Turn Off')
            else:
                error = 'Invalid Credentials. Please try again.'
        return render_template('camera.html', error=error)
    else:
        redirect(url_for('index'))


@app.route('/camera2', methods=['GET', 'POST'])
def camera2():
    error = None
    if g.user:
        if request.method == 'POST':
            if request.form['action'] == 'turn on':
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect(('127.0.0.1', 12346))
                s.send('Turn On')
                s.close()
            elif request.form['action'] == 'turn off':
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect(('127.0.0.1', 12346))
                s.send('Turn Off')
            else:
                error = 'Invalid Credentials. Please try again.'
        return render_template('camera.html', error=error)
    else:
        redirect(url_for('index'))


@app.route('/camera3', methods=['GET', 'POST'])
def camera3():
    error = None
    if g.user:
        if request.method == 'POST':
            if request.form['action'] == 'turn on':
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect(('127.0.0.1', 12347))
                s.send('Turn On')
                s.close()
            elif request.form['action'] == 'turn off':
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect(('127.0.0.1', 12347))
                s.send('Turn Off')
            else:
                error = 'Invalid Credentials. Please try again.'
        return render_template('camera.html', error=error)
    else:
        redirect(url_for('index'))




@app.before_request
def before_request():
    if request.remote_addr == '192.168.1.35':
        g.user = None
        if 'user' in session:
            g.user = session['user']
    else:
        abort(403)  # Forbidden


if __name__ == '__main__':
    app.run(host="192.168.1.28", port=44000, debug=True, ssl_context=context)
