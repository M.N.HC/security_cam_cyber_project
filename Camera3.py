# -*- coding: utf-8 -*-
"""
Created on Tue May 14 18:59:42 2019
@author: May Nissan Hacohen.
"""
import datetime
import os
import socket
from multiprocessing import Process
import cv2
import numpy
import psutil
from pushbullet import Pushbullet

CREATE_NO_WINDOW = 0x08000000
HOST = '192.168.1.28'  # Standard loopback interface address (localhost)
PORT = 12345  # Port to listen on (non-privileged ports are > 1023)
MOTION_RECORD_TIME = datetime.timedelta(seconds=10)
PUSHBULLET_API_KEY = "o.QUxhVmbgRqQl28RfpN4NBoENlSD8wh2W"
PUSHBULLET_DEVICE_NAME = "Xiaomi Redmi Note 6 Pro"


def have_motion(frame1, frame2):
    """'
    Checks if there is a motion in the room.
    Get 2 Frames and subtracting them to check.
    '"""

    if frame1 is None or frame2 is None:
        return False
    delta = cv2.absdiff(frame1, frame2)  # Creates the diffrence between the colors.
    thresh = cv2.threshold(delta, 25, 255, cv2.THRESH_BINARY)[1]  # Makes the diffrence more clear.
    return numpy.sum(thresh) > 0


def send_notification():
    """
    send warning alert
    """
    pb = Pushbullet(PUSHBULLET_API_KEY)
    push = pb.push_note("Warning Camera 3", "Someone Is Entering To Your Room!!!")  # Sending Warning to the owner


def push_file(filename):
    """
    send video file and delete it
    """
    print "Sending", filename
    pushbullet = Pushbullet(PUSHBULLET_API_KEY)
    my_device = pushbullet.get_device(PUSHBULLET_DEVICE_NAME)
    file_data = pushbullet.upload_file(open(filename, "rb"), filename)
    pushbullet.push_file(device=my_device, **file_data)
    print "Sent!"
    try:
        os.remove("Motion_Video Camera 3.avi")
    except Exception as e1:
        print e1
    print "File Deleted  " + datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")  # Deleting the video file


def draw_detections(img, rects, thickness = 1):
    """'
    the the shape around the human and alert about human.
    '"""
    for x, y, w, h in rects:
        # the HOG detector returns slightly larger rectangles than the real objects.
        # so we slightly shrink the rectangles to get a nicer output.
        pad_w, pad_h = int(0.15*w), int(0.05*h)
        cv2.rectangle(img, (x+pad_w, y+pad_h), (x+w-pad_w, y+h-pad_h), (0, 255, 0), thickness)
        print "Motion!", datetime.datetime.now()


def main_camera():
    hog = cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())  # The human detector.
    cap = cv2.VideoCapture(0)  # Initializing The Camera (The Parameter Is Choosing The Camera).
    frame_size = (int(cap.get(3)), int(cap.get(4)))
    fourcc = cv2.VideoWriter_fourcc(*"XVID")  # Video format
    prev_frame = None
    last_motion1 = None
    motion_filename = None
    motion_file = None

    while cap.isOpened():
        now = datetime.datetime.now()
        success, frame = cap.read()
        assert success, "failed reading frame"
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frame_gray = cv2.GaussianBlur(frame_gray, (21, 21), 0)
        if have_motion(prev_frame, frame_gray):
            found, w = hog.detectMultiScale(frame, winStride=(8, 8), padding=(32, 32), scale=1.05)
            draw_detections(frame, found)
            if motion_file is None:
                motion_filename = "Motion_Video Camera 3.avi"
                motion_file = cv2.VideoWriter(motion_filename, fourcc, 20.0, frame_size)
                Process(target=send_notification, args=()).start()  # Sending Warning to the owner
            last_motion1 = now
        if motion_file is not None:
            motion_file.write(frame)
            if now - last_motion1 > MOTION_RECORD_TIME:
                motion_file.release()
                motion_file = None
                Process(target=push_file, args=(motion_filename,)).start()  # Sending The Video File.

        prev_frame = frame_gray
        cv2.imshow('frame1', frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):  # Stopping The Camara.
            break

    cap.release()
    cv2.destroyAllWindows()


def loop(flag, pid, s):
    proc = ''
    conn, addr = s.accept()
    command = conn.recv(1024)
    conn.close()
    if command == 'Turn On' and flag == 0:
        flag = 1
        proc = Process(target=main_camera)
        proc.start()
        pid = proc.pid
    elif command == 'Turn Off' and flag == 1:
        flag = 0
        p = psutil.Process(pid)
        p.terminate()  # or p.kill()
        return loop(flag, pid, s)
    return loop(flag, pid, s)


def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('localhost', 12347))
    s.listen(1)
    loop(0, '', s)


if __name__ == "__main__":
    main()
